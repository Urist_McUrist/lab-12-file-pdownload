#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libsocket/libinetsocket.h>

FILE * connect_to_server();
//displays the menu
void menu();
//helper method to receive input
char get_choice();
//calls the server to list file names and sizes
void list_files(FILE *s);
//downloads a file on a server
void download(FILE *s);
//exits the program
void quit(FILE *s);
//prints out a progress bar while downloading
char * drawProgressBar(int current, int total, int length);
//checks if a file already exists and prompts the user if they want to overwrite
int overwriteCheck(char * fileName);

int main()
{
    // Connect to the server
    FILE * runwire = connect_to_server();
    
    //Call the hello prompt on startup to clear the welcome message
    fprintf(runwire, "HELO\n");
    char garbage[50];
    fgets(garbage, 50, runwire);
    fgets(garbage, 50, runwire);
    free(garbage);
    
    while(1){
        //loop to draw the menu, prompt the user and then execute commands.
        menu();
        char choice = get_choice();
        switch(choice)
        {
            case 'l':
            case 'L':
                list_files(runwire);
                break;
            
            case 'd':
            case 'D':
                download(runwire);
                break;
                
            case 'q':
            case 'Q':
                quit(runwire);
                exit(0);
                break;
                
            default:
                printf("Choice must be d, l, or q\n");
        }
    }
    quit(runwire);
}

/*
 * Connect to server. Returns a FILE pointer that the
 * rest of the program will use to send/receive data.
 */
FILE * connect_to_server(){
    //create the socket using libsocket library
    int socketNum = create_inet_stream_socket("runwire.com", "1234", LIBSOCKET_IPv4, 0);
    //error if the socket number couldn't be created
    if(socketNum == -1){
        fprintf(stderr, "Couldn't Connect to server\n");
        exit(1);
    }
    //open a fileopinter using he socket as a source
    FILE * runwire = fdopen(socketNum, "r+");
    if(!runwire){
        fprintf(stderr, "Could not convert socket number\n");
        exit(2);
    }
    
    return runwire;
}

/*
 * Display menu of choices.
 */
void menu()
{
    printf("(L)ist files\n");
    printf("(D)ownload a file\n");
    printf("(Q)uit\n");
    printf("\n");
}

/*
 * Get the menu choice from the user. Allows the user to
 * enter up to 100 characters, but only the first character
 * is returned.
 */
char get_choice()
{
    printf("Your choice: ");
    char buf[100];
    scanf("%s", buf);
    return buf[0];
}

/*
 * Display a file list to the user.
 */
void list_files(FILE *s)
{
    //prompt the server for the file list
    fprintf(s, "LIST\n");
    
    char line[1000];
    //use string compare to check that the line isn't "." meaning EOF
    while (strcmp(fgets(line, 1000, s), ".\n") != 0 ){
        //use string compare to skip the first line that is just "+OK"
        if(!strcmp(line, "+OK\n")){
            continue;
        }
        printf("%s", line);
    }
    printf("\n");
}

/*
 * Download a file.
 * Prompt the user to enter a filename.
 * Download it from the server and save it to a file with the
 * same name.
 */
void download(FILE *s)
{
    //prompt user for which file to donwload
    printf("File to download: ");
    char filename[50];
    scanf("%s", filename);
    
    //check if the file already exists on disk
    if(!overwriteCheck(filename)){
        return;
    }
    
    char line[1000];
    int length;
    
    //propmt server for the size of the file
    fprintf(s, "SIZE %s\n", filename);
    
    //check to see if the file exists on the server
    //first char of an error code is "-"
    fgets(line, 1000, s);
    if(line[0] == '-'){
        printf("File does not exist\n");
        return;
    }
    
    //skip the return code (line+4) and set the length variable
    sscanf(line+4, "%d", &length);
    printf("Bytes to Download: %d\n", length);
    
    //create the file on disk to recieve the data
    FILE *out = fopen(filename, "w");
    if(!out){
        printf("Couldn't create %s\n", filename);
        exit(1);
    }
    
    
    //prompt the server to send the data
    fprintf(s, "GET %s\n", filename);
    //this line skips the return code
    fgets(line, 1000, s);
    //recData = recieved data, the length of the line is added to it every loop
    for(int recData = 0; recData < length; recData += strlen(line)){
        fgets(line, 1000, s);
        
        //use the drawProgressBar method
        //drawProgressBar uses malloc so freeing the string afterwards is necessary
        char *progressBar = drawProgressBar(recData, length, 20);
        printf("%s", progressBar);
        free(progressBar);
        
        fwrite(line, sizeof(char), strlen(line), out);
    }
    
    fclose(out);
}



char * drawProgressBar(int current, int total, int length){
    
    //you can't have a negative long bar, silly
    if(length <= 0){
        fprintf(stderr, "drawProgressBar() must have a positive length\n");
        exit(1);
    }
    
    //length + 3 for end caps and new line
    char * bar = malloc(length + 3);
    bar[0] = '<';
    bar[length + 1] = '>';
    bar[length + 2] = '\n';
    
    //without the final number the bar would never actually display as full,
    //so for the last 2% it displays completely filled
    int final = total - (total * 0.02);
    //divide the total amount of data by the length to find how much each 
    //tick represents
    int increment = total / length;
    //find progress by dividing current value by increment
    int hashmarks = current / increment;
    //dashes must be the rest of the line 
    int dashes = length - hashmarks;
    
    //check if amount has passed threshold to display a full bar
    if(current > final){
        for(int i = 1; i < length + 1; i++){
            bar[i] = '#';
        }
        return bar;
    }
    
    //set the hashmarks on the bar
    for(int i = 1; i < hashmarks + 1; i++){
        bar[i] = '#';
    }

    //starting at the hashmarks fill the rest of the bar with dashes
    for(int i = hashmarks + 1; i < length + 1; i++){
        bar[i] = '-';
    }
    return bar;
}

int overwriteCheck(char * fileName){
    FILE *check = fopen(fileName, "r");
    if(check){
        printf("File already exists, overwrite? Y/N\n");
        char response = get_choice();
        
        int waiting = 1;
        while(1){
            switch(response){
                case 'Y':
                case 'y':
                    return 1;
                
                case 'N':
                case 'n':
                    return 0;
                
                default:
                    printf("Type 'Y' or 'N' to continue\n");
            }
        }
    }
    else{
        //file doesn't exist
        return 1;
    }
}

//Close the connection to the server.
void quit(FILE *s)
{
    fclose(s);
}